create table Cliente
(
	id_cliente int not null auto_increment,
	nombres varchar(120) not null,
        apellidos varchar(120) not null,
	fecha_nacimiento datetime not null,
	carnet_identidad int not null,
	domicilio varchar(120) not null,
	numero_celular int not null,
        usuario int not null unique,
	password varchar(150) not null,
	primary key (id_cliente)
) ENGINE INNODB;

create table Raza
(
	id_raza int not null auto_increment,
	nombre_raza varchar(120) not null,
        tama�o varchar(120) not null,
        debilidades varchar(120) not null,
	primary key (id_raza)
) ENGINE INNODB;


create table Mascota
(
	id_mascota int not null auto_increment,
	nombre_mascota varchar(120) not null,
        tipo_mascota varchar(50) not null,
        edad_mascota int not null,
        color_mascota varchar(30) not null,
        id_cliente int not nulL,
        id_raza int not null,
	primary key (id_mascota),
        Foreign key (id_raza) references Raza(id_raza),
        Foreign key (id_cliente) references Cliente(id_cliente)
) ENGINE INNODB;

create table Enfermedad
(
	id_enfermedad int not null auto_increment,
	nombre_enfermedad varchar(120) not null,
        medicamentos varchar(120) not null,
	primary key (id_enfermedad),
) ENGINE INNODB;

create table Tratamiento
(
	id_tratamiento int not null auto_increment,
	nombre_tratamiento varchar(60) not null,
	indice_tratamiento int not null,
        descripcion varchar(60) not null,
	costo numeric(10,2) not null,
        id_enfermedad int not null,
        id_mascota int not null,
	primary key (id_tratamiento),
	Foreign key (id_enfermedad) references Enfermedad(id_enfermedad),
        Foreign key (id_mascota) references Mascota(id_mascota)
) ENGINE INNODB;

create table Consulta
(
	id_consulta int not null auto_increment,
	dia varchar(50) not null,
        fecha datetime not null,
        hora varchar(50) not null, 
        id_cliente int not nulL,
        id_tratamiento int not null,
	primary key(id_consulta),
	Foreign key(id_cliente) references Cliente(id_cliente),
        Foreign key(id_tratamiento) references Tratamiento(id_tratamiento)
) ENGINE INNODB;

create table Tratamiento_enfermedad
(
    id_tratamiento int not null,
    id_enfermedad int not null,
    Foreign key (id_enfermedad) references Enfermedad(id_enfermedad),
    Foreign key(id_tratamiento) references Tratamiento(id_tratamiento),
    medicamentos_especificos varchar(200) not null
) ENGINE=INNODB;