<?php
	require_once("../ClasesAdmin/Conexion.php");
	require_once("../ClasesAdmin/Consulta.php");
	require_once("../ClasesAdmin/Cliente.php");
	require_once("../ClasesAdmin/Tratamiento.php");
	require_once("../ClasesAdmin/Mascota.php");

	session_start();
	$con = new Conexion();
	$objcon = new Consulta($con);
	$op = 1;
	$mensaje = "";
	$id_cliente = "1";
	$id_tratamiento = "1";
	$id_consulta = "";
	$dia = "";
	$fecha = "";
	$hora = "";

$nombres = "";
if(!isset($_SESSION["usuario"]))
  header("location:principalAdmin.php");
else
	$nombres = $_SESSION["usuario"];


	if(isset($_GET["op"]))
	{
		$op = $_GET["op"];

		if(isset($_GET["id"]))
		{
			$id_consulta = $_GET["id"];
		}
		if(isset($_GET["d"]))
		{
			$dia = $_GET["d"];
		}
		if(isset($_GET["f"]))
		{
			$fecha = $_GET["f"];
		}
		if(isset($_GET["e"]))
		{
			$edad_mascota = $_GET["e"];
		}
		if(isset($_GET["h"]))
        {
        	$hora = $_GET["h"];
        }
		if(isset($_GET["icl"]))
		{
			$id_cliente = $_GET["icl"];
		}
		if(isset($_GET["it"]))
        {
        	$id_tratamiento = $_GET["it"];
        }
	}
	if(isset($_POST["btncancelar"]))
	{
		$op = 1;
		$mensaje = "";
		$id_consulta = "";
		$dia = "";
		$fecha = "";
		$hora = "";
		$id_cliente = "1";
		$id_tratamiento = "1";
		header("location: registroConsulta.php");
	}
	if(isset($_POST["btnaceptar"]))
{
	
	if($_POST["txtop"] == 1)
	{
		$dia = $_POST["txtdia"];
		$fecha = $_POST["txtfecha"];
		$hora = $_POST["txthora"];
		$id_tratamiento = $_POST["cboTratamiento"];
		$id_cliente = $_POST["cboCliente"];


		$objcon->set_dia($dia);
		$objcon->set_fecha($fecha);
		$objcon->set_hora($hora);
		$objcon->set_id_cliente($id_cliente);
		$objcon->set_id_tratamiento($id_tratamiento);
		if($objcon->guardar())
		{
			
			$mensaje = "Consulta Registrada Correctamente";
			$op = 1;
			$id_consulta = "";
            $dia = "";
            $fecha = "";
            $hora = "";
            $id_cliente = "1";
            $id_tratamiento = "1";
		}
		else
			$mensaje="Error al guardar la Consulta";
	}
	else
	{
		if($_POST["txtop"] == 2)
		{
			$id_consulta=$_POST["txtid"];
			$dia = $_POST["txtdia"];
            $fecha = $_POST["txtfecha"];
            $hora = $_POST["txthora"];
            $id_tratamiento = $_POST["cboTratamiento"];
            $id_cliente = $_POST["cboCliente"];

			$objcon->set_dia($dia);
            $objcon->set_fecha($fecha);
            $objcon->set_hora($hora);
            $objcon->set_id_cliente($id_cliente);
            $objcon->set_id_tratamiento($id_tratamiento);

			if($objcon->modificar())
			{
				$mensaje = "Consulta Modificada Correctamente";
				$op = 1;
				$id_consulta = "";
                $dia = "";
                $fecha = "";
                $hora = "";
                $id_cliente = "1";
                $id_tratamiento = "1";
			}
			else
				$mensaje="Error al modificar la consulta";
		}
		else
		{
			if($_POST["txtop"] == 3)
			{
				$id_consulta = $_POST["txtid"];
				$objcon->set_id_mascota($id_consulta);

				if($objcon->eliminar())
				{					
					$mensaje = "Consulta Eliminada Correctamente";
					$op = 1;
					$id_consulta = "";
                    $dia = "";
                    $fecha = "";
                    $hora = "";
                    $id_cliente = "1";
                    $id_tratamiento = "1";
				}
				else
					$mensaje="Error al eliminar la Consulta";
			}
		}
	}
}
if (isset($_POST["btncerrar"])) {
  header("location:salirAdmin.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Veterinaria Guardián Leal</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="skin-green-light" style="height: auto;" background="fondo2.jpg">
<header class="main-header">
    <div class="wr">
        <nav class="navbar navbar-static-top" style="margin-left: 0px; background-color: rgba(80,194,54,0.4)">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i> Veterinaria Guardián Leal</i>
                        </a>
                    <li>
                        <a href="registroCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"> Cliente</i>
                        </a>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a href="registroMascota.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tag">Mascota</i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroTratamiento.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa  fa-briefcase"> Tratamiento </i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroConsulta.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-calendar-plus-o"> Consulta</i>
                        </a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="user1.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"> Admin</span>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="salirAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-close"> Salir</i>
                        </a>
                    </li>
            </div>
        </nav>
</header>
<div>
    <section class="content-header">
        <h1>
            Veterinaria Guardián Leal
            <small>Registro de Mascota</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Registro de Mascota</a></li>
        </ol>
    </section>
</div>
		<div class="container" align="center">
            <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 250px 50px 250px;">
			<h1 style="color: rgba(0,0,0,0.5); font-size: 25px">Registrar Consultas</h1>
				<div>
				<form name="form1" method="post" action="registroConsulta.php" enctype="multipart/form-data">
					<div>
					   <table>
					      <tr>
					          <td width="150" style="font-size: 20px">Dia:</td>
					              <td height="45"><input type="text" class="form-control" name="txtdia" placeholder="Escriba el dia" value="<?php echo $dia; ?>"></td>
							</tr>
							<tr>
								<td width="150" style="font-size: 20px">Fecha: </td>
							 	<td height="45"><input type="text" class="form-control" name="txtfecha" placeholder="Escriba la fecha" value="<?php echo $fecha; ?>"></td>
							</tr>
							<tr>
							   <td width="150" style="font-size: 20px">Hora: </td>
							       <td height="45"><input type="text" class="form-control" name="txthora" placeholder="Escriba la hora" value="<?php echo $hora; ?>"></td>
							       </tr>
							<tr>
								<td height="45" width="150" style="font-size: 20px">Cliente: </td>
								<td>
									<?php
										$objcli = new Cliente($con);
										$objmascota = new Mascota($con);
										$objmascota->generarCombocl('cboCliente',$id_cliente);
									 ?>
								</td>
							</tr>
							<tr>
							<td height="45" width="150" style="font-size: 20px">Tratamiento: </td>
							<td>
							<?php
							$objcon = new Consulta($con);
							$objcon->generarCombotr('cboTratamiento',$id_tratamiento);
							?>
							</td>
							</tr>
							<tr>
								<td colspan="2" height="45" align="center">
									<input style="width: 100px; margin-right: 50px" type="submit" class="btn btn-success" name="btnaceptar" title="Aceptar la operacion a realizar" value="Aceptar"/>
									<input style="width: 100px; margin-right: 50px" type="submit" name="btncancelar" class="btn btn-danger" title="Borrar los datos ingresados" value="Cancelar"/>
									<input type="hidden" name="txtop" value="<?php echo $op; ?>" style="width: 10px">
								</td>
							</tr>
						</table>
					</div>
                    <div align="center">
                        <br>
                        <span style="color: #00a157; font-size: 18px"><?php echo $mensaje; ?></span>
                    </div>
					</form>
                </div>
            </div>
            <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 250px 50px 250px;">
					<form name="form2" method="post" action="registroConsulta.php" enctype="multipart/form-data">
					<div>
						<h2 style="color: rgba(0,0,0,0.5); font-size: 25px">Buscar Registros de Consultas</h2>
						<table>
							<tr>
								<td width="255">
								<input type="text" name="txtbuscar" class="form-control" placeholder="Escriba los registros a buscar" style="width:250px">
								</td>
								<td>
								<input type="submit" class="btn btn-success" name="btnbuscar" value="Buscar" title="Buscar los registros de consulta">
                                    <input type="submit" class="btn btn-success" name="btnmensaje" value="Enviar Mensaje" title="Envio de Mensaje">
								</td>
							</tr>
						</table>
						<h4 style="color: rgba(0,0,0,0.5); font-size: 25px">Resultados de la Busqueda</h4>
						<div class="table-responsive" style="color: black; background: darkseagreen; border-bottom-color: #00a157">
							<?php
								if(isset($_POST["btnbuscar"]))
								{
									$resultado=$objcon->buscar($_POST["txtbuscar"]);
									$objcon->mostrar($resultado);
								}
							?>
						</div>
					</div>
					</form>
				</div>
        </div>
		</div>
		</font>
</body>
</html>