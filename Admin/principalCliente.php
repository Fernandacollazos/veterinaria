<?php
require_once("../ClasesAdmin/Conexion.php");
require_once("../ClasesAdmin/Tratamiento.php");
require_once("../ClasesAdmin/Mascota.php");
require_once("../ClasesAdmin/Consulta.php");

session_start();
$con = new Conexion();
$objtra = new Tratamiento($con);
$objcon= new Consulta($con);
$nombres = "";
if(isset($_SESSION["usuario"]))
{
	$nombres = $_SESSION["nombres"];
}
else
{
header("location:loginCliente.php");
}

if (isset($_POST["btncerrar"])) {
  header("location:salirCliente.php");
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Veterinaria Guardián Leal</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="skin-blue-light" style="height: auto;" background="fondo3.jpg">
<header class="main-header">
    <div class="wr">
        <nav class="navbar navbar-static-top" style="margin-left: 0px; background-color: rgba(10,47,194,0.24)">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="principalCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i> Veterinaria Guardián Leal</i>
                        </a>
                    <li class="dropdown user user-menu">
                        <a href="principalCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="user1.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $nombres; ?></span>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="salirCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-close"> Salir</i>
                        </a>
                    </li>
            </div>
        </nav>
    </div>
</header>
<div>
    <section class="content-header">
        <h1>
            Veterinaria Guardián Leal
            <small>Cliente</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Cliente</a></li>
        </ol>
    </section>
</div>
<div class="container" align="center">
    <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 200px 50px 200px;">
        <h1 style="color: rgba(0,0,0,0.5); font-size: 25px">Tratamientos Realizados en la Veterinaria</h1>
        <div>
            <form name="form2" method="post" action="principalCliente.php" enctype="multipart/form-data">
                <div>
                    <div class="table-responsive" style="color: black; background:#81bfde; border-bottom-color: #00a157">
                        <?php
                        $resultadosTratamientos = $objtra->buscarPorCliente($_SESSION["usuario"]);
                        print_r($resultadosTratamientos );
                        ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container" align="center">
        <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 200px 50px 200px;">
            <h1 style="color: rgba(0,0,0,0.5); font-size: 25px">Consultas Realizadas en la Veterinaria</h1>
            <div>
                <form name="form2" method="post" action="principalCliente.php" enctype="multipart/form-data">
                    <div>
                        <div class="table-responsive" style="color: black; background:#81bfde; border-bottom-color: #00a157">
                            <?php
                            $resultadosConsultas = $objcon->buscarConsultaPorCliente($_SESSION["usuario"]);
                            print_r($resultadosConsultas );
                            ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
