<?php
	require_once("../ClasesAdmin/Conexion.php");
	require_once("../ClasesAdmin/Tratamiento.php");
	require_once("../ClasesAdmin/Mascota.php");
	require_once("../ClasesAdmin/Enfermedad.php");

	session_start();
	$con = new Conexion();
	$objtra = new Tratamiento($con);
	$op = 1;
	$mensaje = "";
	$id_tratamiento = "1";
	$id_enfermedad = "1";
	$id_mascota = "";
	$nombre_tratamiento = "";
	$indice_tratamiento = "";
	$descripcion = "";
	$costo = "";

$nombres = "";
if(!isset($_SESSION["usuario"]))
  header("location:principalAdmin.php");
else
	$nombres = $_SESSION["usuario"];

	if(isset($_GET["op"]))
	{
		$op = $_GET["op"];

		if(isset($_GET["id"]))
		{
			$id_tratamiento = $_GET["id"];
		}
		if(isset($_GET["n"]))
		{
			$nombre_tratamiento = $_GET["n"];
		}
		if(isset($_GET["i"]))
		{
			$indice_tratamiento = $_GET["i"];
		}
		if(isset($_GET["d"]))
		{
			$descripcion = $_GET["d"];
		}
		if(isset($_GET["c"]))
        {
        	$costo = $_GET["c"];
        }
		if(isset($_GET["ien"]))
		{
			$id_enfermedad = $_GET["ien"];
		}
		if(isset($_GET["im"]))
        {
        	$id_mascota = $_GET["im"];
        }
	}
	if(isset($_POST["btncancelar"]))
	{
		$op = 1;
		$mensaje = "";
		$id_tratamiento = "";
		$nombre_tratamiento = "";
		$indice_tratamiento = "";
		$descripcion = "";
		$costo = "";
		$id_enfermedad = "1";
		$id_mascota = "1";
		header("location: registroTratamiento.php");
	}
	if(isset($_POST["btnaceptar"]))
{
	if($_POST["txtop"] == 1)
	{
		$nombre_tratamiento = $_POST["txtnombre"];
		$indice_tratamiento = $_POST["txtindice"];
		$descripcion = $_POST["txtdescripcion"];
		$costo = $_POST["txtcosto"];
		$id_enfermedad = $_POST["cboEnfermedad"];
		$id_mascota = $_POST["cboMascota"];


		$objtra->set_nombre_tratamiento($nombre_tratamiento);
		$objtra->set_indice_tratamiento($indice_tratamiento);
		$objtra->set_descripcion($descripcion);
		$objtra->set_costo($costo);
		$objtra->set_id_enfermedad($id_enfermedad);
		$objtra->set_id_mascota($id_mascota);

		if($objtra->guardar())
		{
			
			$mensaje = "Tratamiento Registrado Correctamente";
			$op = 1;
			$id_tratamiento = "";
			$nombre_tratamiento = "";
			$indice_tratamiento = "";
			$descripcion = "";
			$costo = "";
			$id_enfermedad = "1";
			$id_mascota = "1";
		}
		else
			$mensaje="Error al Guardar el Tratamiento";
	}
	else
	{
		if($_POST["txtop"] == 2)
		{
			$nombre_tratamiento = $_POST["txtnombre"];
            $indice_tratamiento = $_POST["txtindice"];
            $descripcion = $_POST["txtdescripcion"];
            $costo = $_POST["txtcosto"];
            $id_enfermedad = $_POST["cboEnfermedad"];
            $id_mascota = $_POST["cboMascota"];

			$objtra->set_nombre_tratamiento($nombre_mascota);
            $objtra->set_indice_tratamiento($indice_tratamiento);
            $objtra->set_descripcion($descripcion);
            $objtra->set_costo($costo);
            $objtra->set_id_enfermedad($id_enfermedad);
            $objtra->set_id_mascota($id_mascota);

			if($objtra->modificar())
			{
				$mensaje = "Tratamiento Modificado Correctamente";
				$op = 2;
				$id_tratamiento = "";
                $nombre_tratamiento = "";
                $indice_tratamiento = "";
                $descripcion = "";
                $costo = "";
                $id_enfermedad = "1";
                $id_mascota = "1";
			}
			else
				$mensaje="Error al Modificar el Tratamiento";
		}
		else
		{
			if($_POST["txtop"] == 3)
			{
				$id_tratamiento = $_POST["txtid"];
				$objtra->set_id_tratamiento($id_tratamiento);

				if($objtra->eliminar())
				{					
					$mensaje = "Tratamiento Eliminado Correctamente";
					$op = 3;
					$id_tratamiento = "";
                    $nombre_tratamiento = "";
                    $indice_tratamiento = "";
                    $descripcion = "";
                    $costo = "";
                    $id_enfermedad = "1";
                    $id_mascota = "1";
				}
				else
					$mensaje="Error al Eliminar el Tratamiento";
			}
		}
	}
}
if (isset($_POST["btncerrar"])) {
  header("location:salirAdmin.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Veterinaria Guardián Leal</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="skin-green-light" style="height: auto;" background="fondo2.jpg">
<header class="main-header">
    <div class="wr">
        <nav class="navbar navbar-static-top" style="margin-left: 0px; background-color: rgba(80,194,54,0.4)">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i> Veterinaria Guardián Leal</i>
                        </a>
                    <li>
                        <a href="registroCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"> Cliente</i>
                        </a>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a href="registroMascota.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tag">Mascota</i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroTratamiento.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa  fa-briefcase"> Tratamiento </i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroConsulta.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-calendar-plus-o"> Consulta</i>
                        </a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="user1.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"> Admin</span>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="salirAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-close"> Salir</i>
                        </a>
                    </li>
            </div>
        </nav>
</header>
<div>
    <section class="content-header">
        <h1>
            Veterinaria Guardián Leal
            <small>Registro de Mascota</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Registro de Mascota</a></li>
        </ol>
    </section>
</div>
<div class="container" align="center">
    <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 250px 50px 250px;">
			<h1 style="color: rgba(0,0,0,0.5); font-size: 25px"> Registros de Tratamientos</h1>
				<div>
				<form name="form1" method="post" action="registroTratamiento.php" enctype="multipart/form-data">
					<div>
					   <table>
					      <tr>
					          <td width="200" style="font-size: 20px">Nombre Tratamiento:</td>
					              <td height="45"><input type="text" class="form-control" name="txtnombre" placeholder="Escriba el nombre" value="<?php echo $nombre_tratamiento; ?>"></td>
							</tr>
							<tr>
								<td width="200" style="font-size: 20px">Indice Tratamiento:</td>
							 	<td height="45"><input style="width: 200px" type="text" type="text" class="form-control" name="txtindice" placeholder="Escriba el tipo de mascota" value="<?php echo $indice_tratamiento; ?>"></td>
							</tr>
							<tr>
							   <td width="200" style="font-size: 20px">Descripcion: </td>
							       <td height="45"><input type="text" class="form-control" name="txtdescripcion" placeholder="Escriba una breve descripcion" value="<?php echo $descripcion; ?>"></td>
							       </tr>
							<tr>
								<td width="200" style="font-size: 20px">Costo: </td>
							 	<td height="45"><input type="text" class="form-control" name="txtcosto" placeholder="Escriba el costo" value="<?php echo $costo; ?>"></td>
							</tr>
							<tr>
								<td height="45" width="200" style="font-size: 20px">Enfermedad:</td>
								<td>
									<?php
										$obje = new Enfermedad($con);
										$objtra = new Tratamiento($con);
										$objtra->generarComboen('cboEnfermedad',$id_enfermedad);
									 ?>
								</td>
							</tr>
							<tr>
							<td height="45" width="200" style="font-size: 20px">Mascota:</td>
							<td>
							<?php
							$objtra = new Tratamiento($con);
							$objtra->generarCombom('cboMascota',$id_mascota);
							?>
							</td>
							</tr>
							<tr>
								<td colspan="2" height="45" align="center">
									<input style="width: 100px; margin-right: 50px" type="submit" class="btn btn-success" name="btnaceptar" title="Aceptar la operacion a realizar" value="Aceptar"/>
									<input style="width: 100px; margin-right: 50px" type="submit" name="btncancelar" class="btn btn-danger" title="Borrar los datos ingresados" value="Cancelar"/>
									<input type="hidden" name="txtop" value="<?php echo $op; ?>" style="width: 10px">
								</td>
							</tr>
						</table>
					</div>
                    <div align="center">
                        <br>
                        <span style="color: #00a157; font-size: 18px"><?php echo $mensaje; ?></span>
                    </div>
                </form>
                </div>
    </div>
    <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 200px 50px 200px;">
        <form name="form2" method="post" action="registroTratamiento.php" enctype="multipart/form-data">
            <div>
						<h2 style="color: rgba(0,0,0,0.5); font-size: 25px">Buscar Registros de Tratamientos</h2>
						<table>
							<tr>
								<td width="255">
								<input type="text" name="txtbuscar" class="form-control" placeholder="Escriba el registro a buscar" style="width:250px">
								</td>
								<td>
								<input type="submit" class="btn btn-success" name="btnbuscar" value="Buscar" title="Buscar Tratamiento">
								</td>
							</tr>
						</table>
						<h4 style="color: rgba(0,0,0,0.5); font-size: 25px">Resultados de la Busqueda</h4>
						<div class="table-responsive" style="color: black; background: darkseagreen; border-bottom-color: #00a157">
							<?php
								if(isset($_POST["btnbuscar"]))
								{
									$resultado=$objtra->buscar($_POST["txtbuscar"]);
									$objtra->mostrar($resultado);
								}
							?>
						</div>
					</div>
					</form>
				</div>
		</div>
		</font>
</body>
</html>