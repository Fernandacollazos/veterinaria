<?php
require_once("../ClasesAdmin/Conexion.php");
require_once("../ClasesAdmin/Cliente.php");

$cnx= new Conexion();
$objCliente= new Cliente($cnx);
$mensaje="";
$error = "";
if(isset($_POST["btnAceptar"]))
{



    $objCliente->set_nombres($_POST["txtnombres"]);
    $objCliente->set_apellidos($_POST["txtapellidos"]);
    $objCliente->set_fecha_nacimiento($_POST["txtfecha"]);
    $objCliente->set_carnet_identidad($_POST["txtcarnet"]);
    $objCliente->set_domicilio($_POST["txtdomicilio"]);
    $objCliente->set_numero_celular($_POST["txtcelular"]);
    $objCliente->set_usuario($_POST["txtusuario"]);
    $objCliente->set_password($_POST["txtpassword"]);

    if($objCliente->Guardar())
    {

        $mensaje="Cliente Registrado Correctamente";
    }
    else
    {
        $error="Datos Incorrectos";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Veterinaria Guardián Leal</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="skin-green-light" style="height: auto;" background="fondo2.jpg">
<header class="main-header">
    <div class="wr">
        <nav class="navbar navbar-static-top" style="margin-left: 0px; background-color: rgba(80,194,54,0.4)">
            <div class="navbar-custom-menu" >
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i> Veterinaria Guardián Leal</i>
                        </a>
                    <li>
                        <a href="registroCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"> Cliente</i>
                        </a>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a href="registroMascota.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tag">Mascota</i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroTratamiento.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa  fa-briefcase"> Tratamiento </i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroConsulta.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-calendar-plus-o"> Consulta</i>
                        </a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="user1.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"> Admin</span>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="salirAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-close"> Salir</i>
                        </a>
                    </li>
            </div>
        </nav>
</header>
<div>
    <section class="content-header">
        <h1>
            Veterinaria Guardián Leal
            <small>Registro de Cliente</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Registro de Cliente</a></li>
        </ol>
    </section>
</div>
<div class="container" align="center">
    <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 250px 50px 250px;">
    <H1 style="color: rgba(0,0,0,0.5); font-size: 25px">Registro de Clientes Nuevos</H1>
    <form name="form1" method="post" action="registroCliente.php">
        <div>
            <div>
                <table>
                    <tr>
                        <td width="50" style="font-size: 20px">Nombre: </td><td height="40"><input type="text" class="form-control" name="txtnombres" placeholder="Escriba su nombre"/></td>
                    </tr>
                    <tr>
                    <tr>
                        <td width="120" style="font-size: 20px">Apellidos: </td><td height="40"><input type="text" class="form-control" name="txtapellidos" placeholder="Escriba sus Apellidos"/></td>
                    </tr>
                    <tr>
                        <td width="120" style="font-size: 20px">Fecha de Nacimiento:</td><td height="40"> <input style="width: 300px" type="text" class="form-control" name="txtfecha" placeholder="Escriba su fecha de nacimiento"/></td>
                    </tr>
                    <tr>
                    <tr>
                        <td width="120" style="font-size: 20px">Carnet de Identidad:</td><td height="40"> <input type="text" class="form-control" name="txtcarnet" placeholder="Escriba su carnet de identidad"/></td>
                    </tr>
                    <tr>
                        <td width="120" style="font-size: 20px">Direccion:</td> <td height="40"><input type="text" class="form-control" name="txtdomicilio" placeholder="Escriba su domicilio"/></td>
                    </tr>
                    <tr>
                        <td width="120" style="font-size: 20px">Celular:</td> <td height="40"><input type="tel" class="form-control" name="txtcelular" placeholder="Escriba su celular"/></td>
                    </tr>
                    <tr>
                        <td width="120" style="font-size: 20px">Usuario:</td> <td height="40"><input type="text" class="form-control" name="txtusuario" placeholder="Escriba su Usuario"/></td>
                    </tr>
                    <tr>
                        <td width="120" style="font-size: 20px">Password: </td><td height="40"><input type="password" class="form-control" name="txtpassword" placeholder="Escriba su password"/></td>
                    </tr>
                    <tr>
                        <td height="40" width="200" align="center">
                            <br>
                            <input align="center" style="margin-right: 40px; width: 150px"  type="submit" class="btn btn-success" name="btnAceptar" title="Aceptar la operacion a realizar" value="Aceptar"/>
                        </td>
                        <td height="40" width="200" align="center">
                            <br>
                            <input align="center" style="margin-right: 40px; width: 150px"  type="submit" name="btnCancelar" class="btn btn-danger" title="Borrar los datos ingresados" value="Cancelar"/>
                        </td>
                </table>
            </div>
        </div>
        <div align="center">
            <br>
        <span style="color: #00a157; font-size: 18px"><?php echo $mensaje; ?></span>
        <span style="color: #00a157; font-size: 18px"><?php echo $error; ?></span>
        </div>
    </form>
    </div>
    <div class="row" style="margin: 50px 200px 50px 200px; border-color: #00a157">
        <div class="login-box-body" style="background-color: rgba(249,249,249,0.7) ">
            <form name="form2" method="post" action="registroCliente.php" enctype="multipart/form-data">
                <div>
                    <h2 style="color: rgba(0,0,0,0.5); font-size: 25px">Buscar Registros de Clientes</h2>
                    <table>
                        <tr>
                            <td width="255">
                                <input type="text" name="txtbuscar" class="form-control" placeholder="Escriba el cliente a buscar" style="width:250px">
                            </td>
                            <td>
                                <input type="submit" class="btn btn-success" name="btnbuscar" value="Buscar" title="Buscar Registros de Clientes"
                            </td>
                        </tr>
                    </table>
                    <h4  style="color: rgba(0,0,0,0.5); font-size: 25px">Resultados de la Busqueda</h4>
                    <div class="table-responsive">
                        <?php
                        if(isset($_POST["btnbuscar"]))
                        {
                            $resultado=$objCliente->buscar($_POST["txtbuscar"]);
                            $objCliente->mostrar($resultado);
                        }
                        ?>
                    </div>
                </div>
            </form>
        </div>
</div>
</body>
<?php echo $error; ?>
</html>
