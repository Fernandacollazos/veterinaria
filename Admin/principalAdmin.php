<?php
session_start();
$nombres = "";
if(isset($_SESSION["usuario"]))
{
	$nombres = $_SESSION["nombres"];
}
else
{
header("location:loginAdmin.php");
}

if (isset($_POST["btncerrar"])) {
  header("location:salirAdmin.php");
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Veterinaria Guardián Leal</title>
    <meta charset="UTF-8">
   	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="skin-green-light" style="height: auto;" background="fondo2.jpg">
    <header class="main-header">
        <div class="wr">
        <nav class="navbar navbar-static-top" style="margin-left: 0px; background-color: rgba(80,194,54,0.4)">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i> Veterinaria Guardián Leal</i>
                        </a>
                    <li class="dropdown user user-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="user1.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $nombres; ?></span>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="salirAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-close"> Salir</i>
                        </a>
                    </li>
            </div>
        </nav>
    </header>
    <div>
        <section class="content-header">
            <h1 style="color: white">
                Veterinaria Guardián Leal
                <small >Página Principal</small>
            </h1>
            <ol class="breadcrumb">
                <li style="color: white"><a href="#"><i class="fa fa-dashboard"></i> Principal</a></li>
            </ol>
        </section>
        <section>
            <a href="registroCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                <div class="col-md-3 col-sm-6 col-xs-12" style="margin-left: 250px; padding: 20px">
                    <div class="info-box" align="center" style="background-color: rgba(80,194,54,0.4) ">
                        <span class="info-box-icon bg-green-gradient full-opacity-hover">
                            <i class="fa fa-user"></i>
                        </span>
                        <div class="info-box-content" align="left">
                            <span class="info-box-text" style="color: #fffefd; font-size: 25px">Clientes</span>
                            <span class="info-box-number"></span>
                        </div>
                        <div class="card" style=" margin: 10px 0px 0px 0px; background-color: rgba(80,194,54,0.1);">
                            <div class="card-block" style="  background-color: rgba(80,194,54,0.1">
                                <h4 class="card-title" style="color: white"><br><br>Clientes registrados en la Veterinaria</h4>
                                <p class="card-text" style="color: white">En esta sección podra encontrar la gestión de registros y una lista detallada de los mismos</p>
                                <a href="registroCliente.php" class="btn btn-default">Ingresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <a href="registroMascota.php" class="dropdown-toggle" data-toggle="dropdown">
                <div class="col-md-3 col-sm-6 col-xs-12" style="margin-left: 150px; padding: 20px">
                    <div class="info-box" align="center" style="background-color: rgba(20,128,242,0.4) ">
                        <span class="info-box-icon bg-aqua-gradient full-opacity-hover">
                            <i class="fa fa-tag"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text" style="color: #fffefd; font-size: 25px">Mascotas</span>
                            <span class="info-box-number"></span>
                        </div>
                        <div class="card" style=" margin: 10px 0px 0px 0px; background-color: rgba(20,128,242,0.1);">
                            <div class="card-block" style="  background-color: rgba(20,128,242,0.1)">
                                <h4 class="card-title" style="color: white"><br><br>Mascotas registradas en la Veterinaria</h4>
                                <p class="card-text" style="color: white">En esta sección podra encontrar la gestión de mascotas y una lista detallada de las mismas</p>
                                <a href="registroMascota.php" class="btn btn-default">Ingresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </section>
        <section>
            <a href="registroTratamiento.php" class="dropdown-toggle" data-toggle="dropdown">
                <div class="col-md-3 col-sm-6 col-xs-12" style="margin-left: 250px; padding: 20px">
                    <div class="info-box" align="center" style="background-color: rgba(194,26,51,0.4) ">
                        <span class="info-box-icon bg-maroon-gradient full-opacity-hover">
                            <i class="fa  fa-briefcase"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text" style="color: #fffefd; font-size: 25px">Tratamientos</span>
                            <span class="info-box-number"></span>
                        </div>
                        <div class="card" style=" margin: 10px 0px 0px 0px; background-color: rgba(194,26,51,0.1);">
                            <div class="card-block" style="  background-color: rgba(194,26,51,0.1)">
                                <h4 class="card-title" style="color: white"><br><br>Tratamientos Realizados en la Veterinaria</h4>
                                <p class="card-text" style="color: white">En esta sección podra encontrar la gestión de tratamientos y una lista detallada de los mismos</p>
                                <a href="registroTratamiento.php" class="btn btn-default">Ingresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <a href="registroConsulta.php" class="dropdown-toggle" data-toggle="dropdown">
                <div class="col-md-3 col-sm-6 col-xs-12" style="margin-left: 150px; padding: 20px">
                    <div class="info-box" align="center" style="background-color: rgba(157,46,194,0.4) ">
                        <span class="info-box-icon bg-purple-gradient full-opacity-hover">
                            <i class="fa fa-calendar-plus-o"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text" style="color: #fffefd; font-size: 25px">Consultas</span>
                            <span class="info-box-number"></span>
                        </div>
                        <div class="card" style=" margin: 10px 0px 0px 0px; background-color: rgba(157,46,194,0.1);">
                            <div class="card-block" style="  background-color: rgba(157,46,194,0.1)">
                                <h4 class="card-title" style="color: white"><br><br>Consultas Realizadas en la Veterinaria</h4>
                                <p class="card-text" style="color: white">En esta sección podra encontrar la gestión de consultas y una lista detallada de los mismas</p>
                                <a href="registroConsulta.php" class="btn btn-default">Ingresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </section>
    </div>
</body>
</html>