<?php
	require_once("../ClasesAdmin/Conexion.php");
	require_once("../ClasesAdmin/Consulta.php");
	require_once("../ClasesAdmin/Cliente.php");
	require_once("../ClasesAdmin/Tratamiento.php");
	require_once("../ClasesAdmin/Mascota.php");

	session_start();
	$con = new Conexion();
	$objcon = new Consulta($con);
	$op = 1;
	$mensaje = "";
	$id_cliente = "1";
	$id_tratamiento = "1";
	$id_consulta = "";
	$dia = "";
	$fecha = "";
	$hora = "";

$nombres = "";
if(!isset($_SESSION["usuario"]))
  header("location:principalAdmin.php");
else
	$nombres = $_SESSION["usuario"];


	if(isset($_GET["op"]))
	{
		$op = $_GET["op"];

		if(isset($_GET["id"]))
		{
			$id_consulta = $_GET["id"];
		}
		if(isset($_GET["d"]))
		{
			$dia = $_GET["d"];
		}
		if(isset($_GET["f"]))
		{
			$fecha = $_GET["f"];
		}
		if(isset($_GET["e"]))
		{
			$edad_mascota = $_GET["e"];
		}
		if(isset($_GET["h"]))
        {
        	$hora = $_GET["h"];
        }
		if(isset($_GET["icl"]))
		{
			$id_cliente = $_GET["icl"];
		}
		if(isset($_GET["it"]))
        {
        	$id_tratamiento = $_GET["it"];
        }
	}
	if(isset($_POST["btncancelar"]))
	{
    $op = 1;
    $mensaje = "";
    $id_consulta = "";
    $dia = "";
    $fecha = "";
    $hora = "";
    $id_cliente = "1";
    $id_tratamiento = "1";
    header("location: registroConsulta.php");
	}
	if(isset($_POST["btnbuscar"]))
	{
    $objcon->set_dia($dia);
    $objcon->set_fecha($fecha);
    $objcon->set_hora($hora);
    $objcon->set_id_cliente($id_cliente);
    $objcon->set_id_tratamiento($id_tratamiento);
	}
	if (isset($_POST["btncerrar"]))
	{
        header("location:salirCliente.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Veterinaria Guardián Leal</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="skin-blue-light" style="height: auto;" background="fondo3.jpg">
<header class="main-header">
    <div class="wr">
        <nav class="navbar navbar-static-top" style="margin-left: 0px">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="principalCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i> Veterinaria Guardián Leal</i>
                        </a>
                    <li>
                        <a href="visualizacionTratamiento.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-briefcase"> Tratamientos</i>
                        </a>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a href="visualizacionConsulta.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-calendar-plus-o">Consultas</i>
                        </a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="principalCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="user1.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">Fernanda</span>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="salirCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-close"> Salir</i>
                        </a>
                    </li>
            </div>
        </nav>
</header>
<div>
    <section class="content-header">
        <h1>
            Veterinaria Guardián Leal
            <small>Visualización de Tratamientos</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Visualización de Tratamientos</a></li>
        </ol>
    </section>
</div>
<div class="container" align="center">
    <h1> Visualización de Consultas</h1>
				<div>
					<form name="form2" method="post" action="visualizacionConsulta.php" enctype="multipart/form-data">
					<div>
						<h2>Buscar las Consultas a Realizar en la Veterinaria Guardián Leal</h2>
						<table>
							<tr>
								<td width="255">
								<input type="text" name="txtbuscar" class="form-control" placeholder="Escriba la consulta a buscar" style="width:250px">
								</td>
								<td>
								<input type="submit" class="btn btn-success" name="btnbuscar" value="Buscar" title="Buscar la consultas a realizar">
								</td>
							</tr>
						</table>
						<h4>Resultados de la Busqueda de Consultas a Realizar</h4>
						<div class="table-responsive" style="color: black; background:#81bfde; border-bottom-color: #00a157">
							<?php
								if(isset($_POST["btnbuscar"]))
								{
									$resultado=$objcon->buscar($_POST["txtbuscar"]);
									$objcon->mostrar($resultado);
								}
							?>
						</div>
					</div>
					</form>
				</div>
		</div>
		</font>
</body>
</html>