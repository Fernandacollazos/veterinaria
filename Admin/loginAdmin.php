<?php
require_once("../ClasesAdmin/Conexion.php");
require_once("../ClasesAdmin/Admin.php");
$error = "";
session_start();



if(isset($_POST["btnAceptar"]))
{	
	$con= new Conexion();
	$objAdmin= new Admin($con);
	if( $objAdmin->loguear($_POST["txtusuario"],$_POST["txtpassword"]))
	{
		$_SESSION["usuario"] = $objAdmin->get_usuario();
		$_SESSION["nombres"] = $objAdmin->get_nombres();
		$_SESSION["id_admin"] = $objAdmin->get_id_admin();
		header("location:principalAdmin.php");
	}
	else{

		$error = "Datos incorrectos";
	}
}
if(isset($_POST["btnCancelar"]))
{
  header("location:loginAdmin.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html class="no-js" lang="es">
<title>Veterinaria/Inicio de Sesión Administrador</title>
    <meta charset="UTF-8">
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
            <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
            <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
            <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
            <link rel="stylesheet" href="../plugins/morris/morris.css">
            <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
            <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
            <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
            <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <style>
        body
        {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/main.css">

    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body background="fondo2.jpg">
        <div class="login-box">
            <div class="login-logo">
                <b style="color: white">
                    <b style="color: rgba(1,0,5,0.33)">Veterinaria <br>Guardián Leal</b>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body" style="background-color: rgba(80,194,54,0.4) ">
                <p class="login-box-msg">Ingrese su usuario y contraseña para ingresar</p>
                <form id="form1" name="form1" method="post" action="">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" name="txtusuario" placeholder="Usuario">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="txtpassword" placeholder="Contraseña" >
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <input type="submit" name="btnAceptar" class="btn btn-block btn-default" title="Aceptar la operacion a realizar" value="Ingresar" />
                        </div>
                        <!-- /.col -->
                    </div>
                    <div align="center">
                    <span style="color: #00a157; font-size: 18px"><?php echo $error; ?></span>
                    </div>
                </form>
            </div>
        <!-- /.login-box-body -->
        </div>
    </body>
</html>

<?php
  echo $error;
?>


