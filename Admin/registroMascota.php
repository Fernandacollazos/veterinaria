<?php
	require_once("../ClasesAdmin/Conexion.php");
	require_once("../ClasesAdmin/Mascota.php");
	require_once("../ClasesAdmin/Cliente.php");
	require_once("../ClasesAdmin/Raza.php");

	session_start();
	$con = new Conexion();
	$objmas = new Mascota($con);
	$op = 1;
	$mensaje = "";
	$id_cliente = "1";
	$id_raza = "1";
	$id_mascota = "";
	$nombre_mascota = "";
	$tipo_mascota = "";
	$edad_mascota = "";
	$color_mascota = "";
	$nombres = "";
if(!isset($_SESSION["usuario"]))
  header("location:principalAdmin.php");
else
	$nombres = $_SESSION["usuario"];


	if(isset($_GET["op"]))
	{
		$op = $_GET["op"];

		if(isset($_GET["id"]))
		{
			$id_mascota = $_GET["id"];
		}
		if(isset($_GET["n"]))
		{
			$nombre_mascota = $_GET["n"];
		}
		if(isset($_GET["t"]))
		{
			$tipo_mascota = $_GET["t"];
		}
		if(isset($_GET["e"]))
		{
			$edad_mascota = $_GET["e"];
		}
		if(isset($_GET["c"]))
        {
        	$color_mascota = $_GET["c"];
        }
		if(isset($_GET["icl"]))
		{
			$id_cliente = $_GET["icl"];
		}
		if(isset($_GET["ir"]))
        {
        	$id_raza = $_GET["ir"];
        }
	}
	if(isset($_POST["btncancelar"]))
	{
		$op = 1;
		$mensaje = "";
		$id_mascota = "";
		$nombre_mascota = "";
		$tipo_mascota = "";
		$edad_mascota = "";
		$color_mascota = "";
		$id_cliente = "1";
		$id_raza = "1";
		header("location: registroMascota.php");
	}
	if(isset($_POST["btnaceptar"]))
	{
	
	if($_POST["txtop"] == 1)
	{
		$nombre_mascota = $_POST["txtnombre"];
		$tipo_mascota = $_POST["txttipo"];
		$edad_mascota = $_POST["txtedad"];
		$id_raza = $_POST["cboRaza"];
		$color_mascota = $_POST["txtcolor"];
		$id_cliente = $_POST["cboCliente"];


		$objmas->set_nombre_mascota($nombre_mascota);
		$objmas->set_tipo_mascota($tipo_mascota);
		$objmas->set_edad_mascota($edad_mascota);
		$objmas->set_color_mascota($color_mascota);
		$objmas->set_id_cliente($id_cliente);
		$objmas->set_id_raza($id_raza);
		if($objmas->guardar())
		{
			
			$mensaje = "Mascota Registrada Correctamente";
			$op = 1;
			$id_mascota = "";
			$nombre_mascota = "";
			$tipo_mascota = "";
			$edad_mascota = 0;
			$color_mascota = "";
			$id_cliente = "1";
			$id_raza = "1";
		}
		else
			$mensaje="Error al Guardar";
	}
	else
	{
		if($_POST["txtop"] == 2)
		{
			$id_mascota = $_POST["txtid"];
			$nombre_mascota = $_POST["txtnombre"];
			$tipo_mascota = $_POST["txttipo"];
			$edad_mascota = $_POST["txtedad"];
			$color_mascota = $_POST["txtcolor"];
			$id_cliente = $_POST["cboCliente"];
			$id_raza = $_POST["cboRaza"];

			$objmas->set_id_mascota($id_mascota);
			$objmas->set_nombre_mascota($nombre_mascota);
			$objmas->set_tipo_mascota($tipo_mascota);
			$objmas->set_edad_mascota($edad_mascota);
			$objmas->set_color_mascota($color_mascota);
			$objmas->set_id_cliente($id_cliente);
			$objmas->set_id_raza($id_raza);

			if($objmas->modificar())
			{
				$mensaje = "Mascota Modificada Correctamente";
				$op = 1;
				$id_mascota = "";
				$nombre_mascota = "";
				$tipo_mascota = "";
				$edad_mascota = 0;
				$color_mascota = "";
				$id_cliente = "1";
				$id_raza = "1";
			}
			else
				$mensaje="Error al modificar";
		}
		else
		{
			if($_POST["txtop"] == 3)
			{
				$id_mascota = $_POST["txtid"];
				$objmas->set_id_mascota($id_mascota);

				if($objmas->eliminar())
				{					
					$mensaje = "Mascota Eliminada Correctamente";
					$op = 1;
					$id_mascota = "";
					$nombre_mascota = "";
					$tipo_mascota = "";
					$edad_mascota = 0;
					$color_mascota = "";
					$id_cliente = "1";
					$id_raza = "1";
				}
				else
					$mensaje="Error al eliminar";
			}
		}
	}
}
if (isset($_POST["btncerrar"])) {
  header("location:salirAdmin.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Veterinaria Guardián Leal</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="skin-green-light" style="height: auto;" background="fondo2.jpg">
<header class="main-header">
    <div class="wr">
        <nav class="navbar navbar-static-top" style="margin-left: 0px; background-color: rgba(80,194,54,0.4)">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i> Veterinaria Guardián Leal</i>
                        </a>
                    <li>
                        <a href="registroCliente.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"> Cliente</i>
                        </a>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a href="registroMascota.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tag">Mascota</i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroTratamiento.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa  fa-briefcase"> Tratamiento </i>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="registroConsulta.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-calendar-plus-o"> Consulta</i>
                        </a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="principalAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="user1.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"> Admin</span>
                        </a>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="salirAdmin.php" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-close"> Salir</i>
                        </a>
                    </li>
            </div>
        </nav>
</header>
<div>
    <section class="content-header">
        <h1>
            Veterinaria Guardián Leal
            <small>Registro de Mascota</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Registro de Mascota</a></li>
        </ol>
    </section>
</div>
<div class="container" align="center">
    <div class="login-box-body" style="background-color: rgba(249,249,249,0.7); margin: 50px 300px 50px 300px; ">
        <H1 style="color: rgba(0,0,0,0.5); font-size: 25px">Registro de Mascotas</H1>
        <div>
            <form name="form1" method="post" action="registroMascota.php" enctype="multipart/form-data">
                <div>
                    <table>
                        <tr>
                            <td width="200" style="font-size: 20px">Nombre Mascota:</td>
					              <td height="45"><input type="text" class="form-control" name="txtnombre" placeholder="Escriba el nombre" value="<?php echo $nombre_mascota; ?>"></td>
							</tr>
							<tr>
								<td width="50" style="font-size: 20px">Tipo Mascota: </td>
							 	<td height="45"><input style="width: 200px" type="text" class="form-control" name="txttipo" placeholder="Escriba el tipo de mascota" value="<?php echo $tipo_mascota; ?>"></td>
							</tr>
							<tr>
							   <td width="50" style="font-size: 20px">Edad: </td>
							       <td height="45"><input type="text" class="form-control" name="txtedad" placeholder="Escriba la edad de la mascota" value="<?php echo $edad_mascota; ?>"></td>
							       </tr>
							<tr>
								<td width="50" style="font-size: 20px">Color: </td>
							 	<td height="45"><input type="text" class="form-control" name="txtcolor" placeholder="Escriba el color" value="<?php echo $color_mascota; ?>"></td>
							</tr>
							<tr>
								<td width="50" style="font-size: 20px">Cliente: </td>
								<td>
									<?php
										$objcli = new Cliente($con);
										$objmascota = new Mascota($con);
										$objmascota->generarCombocl('cboCliente',$id_cliente);
									 ?>
								</td>
                            </tr>
                        <tr>
                            <td width="50" height="50px" style="font-size: 20px">Raza: </td>
                            <td>
							<?php
							$objraza = new Raza($con);
							$objraza->generarCombor('cboRaza',$id_raza);
							?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="45" align="center">
                                <input style="width: 100px; margin-right: 50px" type="submit" class="btn btn-success" name="btnaceptar" title="Aceptar la operacion a realizar" value="Aceptar"/>
                                <input style="width: 100px; margin-right: 50px" type="submit" name="btncancelar" class="btn btn-danger" title="Borrar los datos ingresados" value="Cancelar"/>
                                <input type="hidden" name="txtop" value="<?php echo $op; ?>" style="width: 10px">
                            </td>
                        </tr>
                    </table>
                </div>
                <div align="center">
                    <br>
                    <span style="color: #00a157; font-size: 18px"><?php echo $mensaje; ?></span>
                </div>
            </form>
        </div>
    </div>
    <div class="row" style="margin: 50px 200px 50px 200px; border-color: #00a157">
        <div class="login-box-body" style="background-color: rgba(249,249,249,0.7) ">
        <form name="form2" method="post" action="registroMascota.php" enctype="multipart/form-data">
            <div>
                <h2 style="color: rgba(0,0,0,0.5); font-size: 25px">Buscar Registros de Mascotas</h2>
                <table>
                    <tr>
                        <td width="255">
                            <input type="text" name="txtbuscar" class="form-control" placeholder="Escriba la mascota a buscar" style="width:250px">
                        </td>
                        <td>
                            <input type="submit" class="btn btn-success" name="btnbuscar" value="Buscar" title="Buscar Registros de Mascotas"
                        </td>
                    </tr>
                </table>
                <h4  style="color: rgba(0,0,0,0.5); font-size: 25px">Resultados de la Busqueda</h4>
                <div class="table-responsive" style="color: black; background: darkseagreen; border-bottom-color: #00a157">
                    <?php
                    if(isset($_POST["btnbuscar"]))
                    {
                        $resultado=$objmas->buscar($_POST["txtbuscar"]);
                        $objmas->mostrar($resultado);
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>