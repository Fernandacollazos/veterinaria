<?php
	class Consulta
	{
		//Atributos
		private $id_consulta;
		private $dia;
		private $fecha;
		private $hora;
		private $id_cliente;
		private $id_tratamiento;
		public $con;

		function Consulta($con)
		{
			$this->con = $con;
			$this->id_consulta = 0;
			$this->dia = "";
			$this->fecha = "";
			$this->hora = "";
			$this->id_cliente = 0;
			$this->id_tratamiento = 0;
		}

		function set_id_consulta($valor)
		{
			$this->id_consulta=$valor;
		}
		function get_id_consulta()
		{
			return $this->id_consulta;
		}

		function set_dia($valor)
		{
			$this->dia=$valor;
		}
		function get_dia()
		{
			return $this->dia;
		}
		function set_fecha($valor)
        {
        	$this->fecha=$valor;
        }
        function get_fecha()
        {
        	return $this->fecha;
        }
		function set_hora($valor)
		{
			$this->hora=$valor;
		}
		function get_hora()
		{
			return $this->hora;
		}
		function set_id_cliente($valor)
		{
			$this->id_cliente=$valor;
		}
		function get_id_cliente()
		{
			return $this->id_cliente;
		}

		function set_id_tratamiento($valor)
		{
			$this->id_tratamiento=$valor;
		}
		function get_id_tratamiento()
		{
			return $this->id_tratamiento;
		}

		function guardar()
		{
			$sql="insert into Consulta values(0,'$this->dia', '$this->fecha', '$this->hora', $this->id_cliente, $this->id_tratamiento)";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function modificar()
		{
			$sql="update Consulta set dia='$this->dia', fecha='$this->fecha', hora = '$this->hora', id_cliente = $this->id_cliente, id_tratamiento = $this->id_tratamiento  where id_consulta = $this->id_consulta";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function eliminar()
		{
			$sql="delete from Consulta where id_consulta=$this->id_consulta";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function buscar($criterio)
		{
			$sql ="select * from Consulta where dia like '%$criterio%'";
			return $this->con->execute($sql);
		}
        function buscarConsultaPorCliente($usuario)
        {
            $sql ="select * from Consulta c INNER JOIN Cliente cl ON c.id_cliente = cl.id_cliente INNER JOIN Tratamiento t ON c.id_tratamiento = t.id_tratamiento   WHERE cl.usuario ='$usuario'";
            $registros = $this->con->execute($sql);

            echo "<table border='2' class='table table-bordered' align='center' style='color: black; background-color:white'> ";
            echo "<tr><th style='width: 40px' align='center'>Id</th><th style='width: 40px' align='center'>Dia</th><th style='width: 40px' align='center'>Fecha</th><th style='width: 40px' align='center'>Hora</th><th style='width: 40px' align='center'>Cliente</th><th style='width: 40px' align='center'>Tratamiento</th></tr>";
            while($reg=$this->con->next($registros))
            {
                $id=$reg[0]; $d=$reg[1]; $f=$reg[2]; $h=$reg[3]; $cl=$reg[7]; $t=$reg[16];
                echo "<tr>";
                echo "<td>$id</td>";
                echo "<td>$d</td>";
                echo "<td>$f</td>";
                echo "<td>$h</td>";
                echo "<td>$cl</td>";
                echo "<td>$t</td>";
                echo "</tr>";
            }
            echo "</table>";
        }
		function mostrar($registros)
		{
			echo "<table border='2' class='table table-bordered' align='center' style='color: black; background-color:white'> ";
			echo "<tr><th style='width: 10px' align='center'>Id</th><th style='width: 10px' align='center'>Dia</th><th style='width: 10px' align='center'>Fecha</th><th style='width: 10px' align='center'>Hora</th><th style='width: 10px' align='center'>Cliente</th><th style='width: 10px' align='center'>Tratamiento</th><th style='width: 10px' align='center'>Modificar</th><th style='width: 10px' align='center'>Eliminar</th></tr>";
			while($reg=$this->con->next($registros))
			{
				$id=$reg[0]; $d=$reg[1]; $f=$reg[2]; $h=$reg[3]; $cl=$reg[4]; $t=$reg[5];
				echo "<tr>";
				echo "<td>$id</td>";
				echo "<td>$d</td>";
				echo "<td>$f</td>";
				echo "<td>$h</td>";
				echo "<td>$cl</td>";
				echo "<td>$t</td>";
				echo "<td><a href='registroConsulta.php?op=2&id=$id&d=$d&f=$f&h=$h&cl=$cl&t=$t $cl=$reg[4];'>Modificar</a></td>";
				echo "<td><a href='registroConsulta.php?op=3&id=$id&d=$d&f=$f&h=$h&cl=$cl&t=$t'>Eliminar</a></td>";
				echo "</tr>";
			}
			echo "</table>";
		}
		public function generarCombotr($nombre, $id_tratamiento)
		{
			$sql = "select * from Tratamiento order by nombre_tratamiento";
			$resultado=$this->con->execute($sql);
			echo "<select name='$nombre' class='form-control'>";
			while($reg=$this->con->next($resultado))
			{
				$id=$reg[0];
				$n=$reg[1];

				if($id==$id_tratamiento)
				{
					echo "<option value='$id' seleted>$n</option>";
				}
				else
				{
					echo "<option value='$id'>$n</option>";
				}
			}
			echo "</select>";
		}
	}
?>