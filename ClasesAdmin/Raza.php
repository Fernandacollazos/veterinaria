<?php
	class Raza
	{
		//Atributos
		private $id_raza;
		private $nombre_raza;
		private $tamaño;
		private $debilidades;
		public $con;

		function Raza($con)
		{
			$this->con = $con;
			$this->id_raza = 0;
			$this->nombre_raza = "";
			$this->tamaño = "";
			$this->debilidades = "";
		}

		function set_id_raza($valor)
		{
			$this->id_raza=$valor;
		}
		function get_id_raza()
		{
			return $this->id_raza;
		}

		function set_nombre_raza($valor)
		{
			$this->nombre_raza=$valor;
		}
		function get_nombre_raza()
		{
			return $this->nombre_raza;
		}

		function set_tamaño($valor)
		{
			$this->tamaño=$valor;
		}
		function get_tamaño()
		{
			return $this->tamaño;
		}

		function set_debilidades($valor)
		{
			$this->debilidades=$valor;
		}
		function get_debilidades()
		{
			return $this->debilidades;
		}
		public function generarCombor($nombre_raza, $id_raza)
                            {
                                $sql = "select * from Raza order by nombre_raza";
                                $resultado=$this->con->execute($sql);
                                echo "<select name='$nombre_raza' class='form-control'>";
                                while($reg=$this->con->next($resultado))
                                {
                                    $id_raza=$reg[0];
                                    $n=$reg[1];

                                    if($id_raza==$id_raza)
                                    {
                                        echo "<option value='$id_raza' seleted>$n</option>";
                                    }
                                    else
                                    {
                                        echo "<option value='$id_raza'>$n</option>";
                                    }
                                }
                                echo "</select>";
                            }
	}
?>