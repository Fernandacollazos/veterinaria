<?php
	class Cliente
	{
		//Atributos
		private $id_cliente;
		private $nombres;
		private $apellidos;
		private $fecha_nacimiento;
		private $carnet_identidad;
		private $domicilio;
		private $numero_celular;
        private $usuario;
        private $password;
		public $con;

		function Cliente($con)
		{
			$this->con = $con;
			$this->id_cliente = 0;
			$this->nombres = "";
			$this->apellidos = "";
			$this->fecha_nacimiento = "";
			$this->carnet_identidad = 0;
			$this->domicilio ="";
			$this->numero_celular = 0;
            $this->usuario = "";
            $this->password = "";
		}

		function set_id_cliente($valor)
		{
			$this->id_cliente=$valor;
		}
		function get_id_cliente()
		{
			return $this->id_cliente;
		}
		function set_nombres($valor)
		{
			$this->nombres=$valor;
		}
		function get_nombres()
		{
			return $this->nombres;
		}
		function set_apellidos($valor)
        {
        	$this->apellidos=$valor;
        }
        function get_apellidos()
        {
        	return $this->apellidos;
        }
		function set_fecha_nacimiento($valor)
		{
			$this->fecha_nacimiento=$valor;
		}
		function get_fecha_nacimiento()
		{
			return $this->fecha_nacimiento;
		}
		function set_carnet_identidad($valor)
        {
        	$this->carnet_identidad=$valor;
        }
        function get_carnet_identidad()
        {
        	return $this->carnet_identidad;
        }
		function set_domicilio($valor)
		{
			$this->domicilio=$valor;
		}
		function get_domicilio()
		{
			return $this->domicilio;
		}
		function set_numero_celular($valor)
		{
			$this->numero_celular=$valor;
		}
		function get_numero_celular()
		{
			return $this->numero_celular;
		}
		function set_usuario($valor)
		{
			$this->usuario=$this->con->validarCaracteres($valor);
		}
		function get_usuario()
		{
			return $this->usuario;
		}
        function set_password($valor)
		{
			$this->password=$this->con->validarCaracteres($valor);
		}
		function get_password()
		{
			return $this->password;
		}

		function Guardar()
		{
			$sql="insert into Cliente values(0,'$this->nombres', '$this->apellidos', '$this->fecha_nacimiento', $this->carnet_identidad, '$this->domicilio', $this->numero_celular,'$this->usuario',MD5('$this->password'))";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function modificar()
		{
			$sql="update Cliente set nombres='$this->nombres', apellidos='$this->apellidos', fecha_nacimiento='$this->fecha_nacimiento', carnet_identidad = $this->carnet_identidad, domicilio = '$this->domicilio', numero_celular = $this->numero_celular, usuario = '$this->usuario', password = '$this->password'  where id_cliente = $this->id_cliente";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function eliminar()
		{
			$sql="delete from Cliente where id_cliente=$this->id_cliente";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		public function validar()
        {
		if($this->nombres=="")
			return false;
		else
			if($this->usuario=="")
				return false;
			else
				if($this->password=="")
					return false;
		return true;
		}
        function buscar($criterio)
        {
            $sql ="select * from Cliente where nombre_cliente like '%$criterio%'";
            return $this->con->execute($sql);
        }
        function mostrar($registros)
        {
            echo "<table border='2' class='table table-bordered' align='center' style='color: black; background-color:white'> ";
            echo "<tr><th style='width: 10px' align='center'>Id</th><th style='width: 10px' align='center'>Nombres</th><th style='width: 10px' align='center'>Apellidos</th><th style='width: 10px' align='center'>Fecha de Nacimiento</th><th style='width: 10px' align='center'>Carnet de Identidad</th><th style='width: 10px' align='center'>Domicilio</th><th style='width: 10px' align='center'>Numero de Celular</th><th style='width: 10px' align='center'>Usuario</th><th>Modificar</th><th>Eliminar</th></tr>";
            while($reg=$this->con->next($registros))
            {
                $id=$reg[0]; $n=$reg[1]; $a=$reg[2]; $fn=$reg[3]; $ci=$reg[4]; $d=$reg[5]; $nc=$reg[6]; $u=$reg[7];
                echo "<tr>";
                echo "<td>$id</td>";
                echo "<td>$n</td>";
                echo "<td>$a</td>";
                echo "<td>$fn</td>";
                echo "<td>$ci</td>";
                echo "<td>$d</td>";
                echo "<td>$nc</td>";
                echo "<td>$u</td>";
                echo "<td><a href='registroCliente.php?op=2&id=$id&n=$n&a=$a&fn=$fn&ci=$ci&d=$d&nc=$nc&u=$u $n=$reg[1];'>Modificar</a></td>";
                echo "<td><a href='registroCliente.php?op=3&id=$id&n=$n&a=$a&fn=$fn&ci=$ci&d=$d&nc=$nc&u=$u'>Eliminar</a></td>";
                echo "</tr>";
            }
            echo "</table>";
        }

		public function loguear($usuario,$password)
		{
		$usuario=$this->con->validarCaracteres($usuario);
		$password=$this->con->validarCaracteres($password);
		$sql= "select * from Cliente where usuario='$usuario' and password=MD5('$password')";
		$resultado=$this->con->execute($sql);
		$registro=$this->con->next($resultado);
		if(isset($registro[0]))
		{
		$this->id_cliente=$registro[0];
		$this->nombres=$registro[1];
		$this->usuario=$registro[7];
		return true;
		}
		else
		return false;
		}
	}
?>