<?php
	class Admin
	{
		//Atributos
		private $id_admin;
		private $nombres;
        private $usuario;
        private $password;
		public $con;

		function Admin($con)
		{
			$this->con = $con;
			$this->id_admin = 0;
			$this->nombres = "";
            $this->usuario = "";
            $this->password = "";
		}

		function set_id_admin($valor)
		{
			$this->id_admin=$valor;
		}
		function get_id_admin()
		{
			return $this->id_admin;
		}

		function set_nombres($valor)
		{
			$this->nombres=$valor;
		}
		function get_nombres()
		{
			return $this->nombres;
		}
		function set_usuario($valor)
		{
			$this->usuario=$this->con->validarCaracteres($valor);
		}
		function get_usuario()
		{
			return $this->usuario;
		}
        function set_password($valor)
		{
			$this->password=$this->con->validarCaracteres($valor);
		}
		function get_password()
		{
			return $this->password;
		}

		public function validar()
		{
		if($this->nombres=="")
			return false;
		else
			if($this->usuario=="")
				return false;
			else
				if($this->password=="")
					return false;
		return true;
		}

		public function loguear($usuario,$password)
		{
            $usuario=$this->con->validarCaracteres($usuario);
            $password=$this->con->validarCaracteres($password);
            $sql= "select * from Admin where usuario='$usuario' and password=MD5('$password')";
            $resultado=$this->con->execute($sql);
            $registro=$this->con->next($resultado);
            echo $registro;
            if(isset($registro[0]))
            {
                $this->id_admin=$registro[0];
                $this->nombres=$registro[1];
                $this->usuario=$registro[2];
                return true;
            }
            else{

            return false;
            }
		}
	}
?>