<?php
	class Enfermedad
	{
		//Atributos
		private $id_enfermedad;
		private $nombre_enfermedad;
		private $medicamentos;
		public $con;

		function Enfermedad($con)
		{
			$this->con = $con;
			$this->id_enfermedad = 0;
			$this->nombre_enfermedad = "";
			$this->medicamentos = "";
		}

		function set_id_enfermedad($valor)
		{
			$this->id_enfermedad=$valor;
		}
		function get_id_enfermedad()
		{
			return $this->id_enfermedad;
		}

		function set_nombre_enfermedad($valor)
		{
			$this->nombre_enfermedad=$valor;
		}
		function get_nombre_enfermedad()
		{
			return $this->nombre_enfermedad;
		}

		function set_medicamentos($valor)
		{
			$this->medicamentos=$valor;
		}
		function get_medicamentos()
		{
			return $this->medicamentos;
		}
	}
?>