<?php
	class Mascota
	{
		//Atributos
		private $id_mascota;
		private $nombre_mascota;
		private $tipo_mascota;
		private $edad_mascota;
		private $color_mascota;
		private $id_cliente;
		private $id_raza;
		public $con;

		function Mascota($con)
		{
			$this->con = $con;
			$this->id_mascota = 0;
			$this->nombre_mascota = "";
			$this->tipo_mascota = "";
			$this->edad_mascota = 0;
			$this->color_mascota = 0;
			$this->id_cliente = 0;
			$this->id_raza = 0;
		}

		function set_id_mascota($valor)
		{
			$this->id_mascota=$valor;
		}
		function get_id_mascota()
		{
			return $this->id_mascota;
		}

		function set_nombre_mascota($valor)
		{
			$this->nombre_mascota=$valor;
		}
		function get_nombre_mascota()
		{
			return $this->nombre_mascota;
		}
		function set_tipo_mascota($valor)
        {
        	$this->tipo_mascota=$valor;
        }
        function get_tipo_mascota()
        {
        	return $this->tipo_mascota;
        }
		function set_edad_mascota($valor)
		{
			$this->edad_mascota=$valor;
		}
		function get_edad_mascota()
		{
			return $this->edad_mascota;
		}

		function set_color_mascota($valor)
		{
			$this->color_mascota=$valor;
		}
		function get_color_mascota()
		{
			return $this->color_mascota;
		}

		function set_id_cliente($valor)
		{
			$this->id_cliente=$valor;
		}
		function get_id_cliente()
		{
			return $this->id_cliente;
		}

		function set_id_raza($valor)
		{
			$this->id_raza=$valor;
		}
		function get_id_raza()
		{
			return $this->id_raza;
		}

		function guardar()
		{
			$sql="insert into Mascota values(0,'$this->nombre_mascota', '$this->tipo_mascota', $this->edad_mascota, '$this->color_mascota', $this->id_cliente, $this->id_raza)";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function modificar()
		{
			$sql="update Mascota set nombre_mascota='$this->nombre_mascota', tipo_mascota='$this->tipo_mascota', edad_mascota = $this->edad_mascota, color_mascota='$this->color_mascota', id_cliente = $this->id_cliente, id_raza = $this->id_raza  where id_mascota = $this->id_mascota";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function eliminar()
		{
			$sql="delete from Mascota where id_mascota=$this->id_mascota";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function buscar($criterio)
		{
			$sql ="select * from Mascota where nombre_mascota like '%$criterio%'";
			return $this->con->execute($sql);
		}
		function mostrar($registros)
		{
			echo "<table border='2' class='table table-bordered' align='center' style='color: black; background-color:white'> ";
			echo "<tr><th style='width: 10px' align='center'>Id</th><th style='width: 10px' align='center'>Nombre Mascota</th><th style='width: 10px' align='center'>Tipo Mascota</th><th style='width: 10px' align='center'>Edad</th><th style='width: 10px' align='center'>Color</th><th style='width: 10px' align='center'>Cliente</th><th style='width: 10px' align='center'>Raza</th><th style='width: 10px' align='center'>Modificar</th><th style='width: 10px' align='center'>Eliminar</th></tr>";
			while($reg=$this->con->next($registros))
			{
				$id=$reg[0]; $n=$reg[1]; $t=$reg[2]; $e=$reg[3]; $c=$reg[4]; $cl=$reg[5]; $r=$reg[6];
				echo "<tr>";
				echo "<td>$id</td>";
				echo "<td>$n</td>";
				echo "<td>$t</td>";
				echo "<td>$e</td>";
				echo "<td>$c</td>";
				echo "<td>$cl</td>";
				echo "<td>$r</td>";
				echo "<td><a href='registroMascota.php?op=2&id=$id&n=$n&t=$t&e=$e&c=$c&cl=$cl&r=$r $id=$reg[0];'>Modificar</a></td>";
				echo "<td><a href='registroMascota.php?op=3&id=$id&n=$n&t=$t&e=$e&c=$c&cl=$cl&r=$r'>Eliminar</a></td>";
				echo "</tr>";
			}
			echo "</table>";
		}
		public function generarCombocl($nombre, $id_cliente)
		{
			$sql = "select * from Cliente order by nombres";
			$resultado=$this->con->execute($sql);
			echo "<select name='$nombre' class='form-control'>";
			while($reg=$this->con->next($resultado))
			{
				$id=$reg[0];
				$n=$reg[1];

				if($id==$id_cliente)
				{
					echo "<option value='$id' seleted>$n</option>";
				}
				else
				{
					echo "<option value='$id'>$n</option>";
				}
			}
			echo "</select>";
		}
	}
?>