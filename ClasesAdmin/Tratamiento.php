<?php
	class Tratamiento
	{
		//Atributos
		private $id_tratamiento;
		private $nombre_tratamiento;
		private $indice_tratamiento;
		private $descripcion;
		private $costo;
		private $id_enfermedad;
		private $id_mascota;
		public $con;

		function Tratamiento($con)
		{
			$this->con = $con;
			$this->id_tratamiento = 0;
			$this->nombre_tratamiento = "";
			$this->indice_tratamiento = 0;
			$this->descripcion = "";
			$this->costo= 0;
			$this->id_enfermedad = 0;
			$this->id_mascota = 0;
		}

		function set_id_tratamiento($valor)
		{
			$this->id_tratamiento=$valor;
		}
		function get_id_tratamiento()
		{
			return $this->id_tratamiento;
		}

		function set_nombre_tratamiento($valor)
		{
			$this->nombre_tratamiento=$valor;
		}
		function get_nombre_tratamiento()
		{
			return $this->nombre_tratamiento;
		}
		function set_indice_tratamiento($valor)
        {
        	$this->indice_tratamiento=$valor;
        }
        function get_indice_tratamiento()
        {
        	return $this->indice_tratamiento;
        }
		function set_descripcion($valor)
		{
			$this->descripcion=$valor;
		}
		function get_descripcion()
		{
			return $this->descripcion;
		}

		function set_costo($valor)
		{
			$this->costo=$valor;
		}
		function get_costo()
		{
			return $this->costo;
		}

		function set_id_enfermedad($valor)
		{
			$this->id_enfermedad=$valor;
		}
		function get_id_enfermedad()
		{
			return $this->id_enfermedad;
		}

		function set_id_mascota($valor)
		{
			$this->id_mascota=$valor;
		}
		function get_id_mascota()
		{
			return $this->id_mascota;
		}

		function guardar()
		{
			$sql="insert into Tratamiento values(0,'$this->nombre_tratamiento', $this->indice_tratamiento, '$this->descripcion', $this->costo, $this->id_enfermedad, $this->id_mascota)";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function modificar()
		{
			$sql="update Tratamiento set nombre_tratamiento='$this->nombre_tratamiento', indice_tratamiento=$this->indice_tratamiento, descripcion = '$this->descripcion', costo=$this->costo, id_enfermedad = $this->id_enfermedad, id_mascota = $this->id_mascota  where id_tratamiento = $this->id_tratamiento";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function eliminar()
		{
			$sql="delete from Tratamiento where id_tratamiento=$this->id_tratamiento";
			$resultado=$this->con->execute($sql);
			if(isset($resultado))
				return true;
			else
				return false;
		}
		function buscar($criterio)
		{
			$sql ="select * from Tratamiento where nombre_tratamiento like '%$criterio%'";
			return $this->con->execute($sql);
		}

        function buscarPorCliente($usuario)
        {
            $sql ="select * from Tratamiento t INNER JOIN Mascota m ON m.id_mascota = t.id_mascota INNER JOIN Cliente c ON c.id_cliente = m.id_cliente INNER JOIN Enfermedad e ON t.id_enfermedad = e.id_enfermedad WHERE c.usuario ='$usuario'";
            $registros = $this->con->execute($sql);

            echo "<table border='2' class='table table-bordered' align='center' style='color: black; background-color:white'>";
            echo "<tr><th style='width: 10px' align='center'>Id</th><th style='width: 10px' align='center'>Nombre Tratamiento</th><th style='width: 10px' align='center'>Indice</th><th style='width: 10px' align='center'>Descripcion</th><th style='width: 10px' align='center'>Costo</th><th style='width: 10px' align='center'>Enfermedad</th><th style='width: 10px' align='center'>Mascota</th></tr>";
            while($reg=$this->con->next($registros))
            {
                $id=$reg[0]; $n=$reg[1]; $i=$reg[2]; $d=$reg[3]; $c=$reg[4]; $en=$reg[24]; $m=$reg[8];
                echo "<tr>";
                echo "<td>$id</td>";
                echo "<td>$n</td>";
                echo "<td>$i</td>";
                echo "<td>$d</td>";
                echo "<td>$c</td>";
                echo "<td>$en</td>";
                echo "<td>$m</td>";
                echo "</tr>";
            }
            echo "</table>";
        }
		function mostrar($registros)
		{
			echo "<table border='2' class='table table-bordered' align='center' style='color: black; background-color:white'>";
			echo "<tr><th style='width: 10px' align='center'>Id</th><th style='width: 10px' align='center'>Nombre Tratamiento</th><th style='width: 10px' align='center'>Indice</th><th style='width: 10px' align='center'>Descripcion</th><th style='width: 10px' align='center'>Costo</th><th style='width: 10px' align='center'>Enfermedad</th><th style='width: 10px' align='center'>Mascota</th><th style='width: 10px' align='center'>Modificar</th><th style='width: 10px' align='center'>Eliminar</th></tr>";
			while($reg=$this->con->next($registros))
			{
				$id=$reg[0]; $n=$reg[1]; $i=$reg[2]; $d=$reg[3]; $c=$reg[4]; $en=$reg[5]; $m=$reg[8];
				echo "<tr>";
				echo "<td>$id</td>";
				echo "<td>$n</td>";
				echo "<td>$i</td>";
				echo "<td>$d</td>";
				echo "<td>$c</td>";
				echo "<td>$en</td>";
				echo "<td>$m</td>";
				echo "<td><a href='registroTratamiento.php?op=2&id=$id&n=$n&i=$i&d=$d&c=$c&en=$en&m=$m $en=$reg[5];'>Modificar</a></td>";
				echo "<td><a href='registroTratamiento.php?op=3&id=$id&n=$n&i=$i&d=$d&c=$c&en=$en&m=$m'>Eliminar</a></td>";
				echo "</tr>";
			}
			echo "</table>";
		}
		public function generarComboen($nombre, $id_enfermedad)
		{
			$sql = "select * from Enfermedad order by nombre_enfermedad";
			$resultado=$this->con->execute($sql);
			echo "<select name='$nombre' class='form-control'>";
			while($reg=$this->con->next($resultado))
			{
				$id=$reg[0];
				$n=$reg[1];

				if($id==$id_enfermedad)
				{
					echo "<option value='$id' seleted>$n</option>";
				}
				else
				{
					echo "<option value='$id'>$n</option>";
				}
			}
			echo "</select>";
		}
		public function generarCombom($nombre_mascota, $id_mascota)
                                    {
                                        $sql = "select * from Mascota order by nombre_mascota";
                                        $resultado=$this->con->execute($sql);
                                        echo "<select name='$nombre_mascota' class='form-control'>";
                                        while($reg=$this->con->next($resultado))
                                        {
                                            $id_mascota=$reg[0];
                                            $n=$reg[1];

                                            if($id_mascota==$id_mascota)
                                            {
                                                echo "<option value='$id_mascota' seleted>$n</option>";
                                            }
                                            else
                                            {
                                                echo "<option value='$id_mascota'>$n</option>";
                                            }
                                        }
                                        echo "</select>";
                                    }
	}
?>